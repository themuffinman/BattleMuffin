using Newtonsoft.Json;

namespace BattleMuffin.Models.Warcraft.Community
{
    /// <summary>
    ///     A guild perk.
    /// </summary>
    public class Perk : IWarcraftModel
    {
        /// <summary>
        ///     Gets or sets the guild level.
        /// </summary>
        [JsonProperty("guildLevel")]
        public int GuildLevel { get; set; }

        /// <summary>
        ///     Gets or sets the spell.
        /// </summary>
        [JsonProperty("spell")]
        public Spell? Spell { get; set; }
    }
}
